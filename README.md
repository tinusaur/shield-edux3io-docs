# Tinusaur Shield EDUx3IO Documentation

Choose from the list below.

## Tinusaur Shield EDUx3IO, Gen:4 mk-II - Assembling Guide

[![Tinusaur Shield EDUx3IO Assembling Guide](Tinusaur_Shield_EDUx3IO_Gen4_mkII_Assembling_Guide.jpg)](https://gitlab.com/tinusaur/shield-edux3io-docs/-/raw/master/Tinusaur_Shield_EDUx3IO_Gen4_mkII_Assembling_Guide.pdf?inline=false)

## Tinusaur Shield EDUx3IO Gen:4 mk-I - Assembling Guide.

- [Tinusaur_Shield_EDUx3IO_Gen4_mkI_Assembling_Guide.pdf](Tinusaur_Shield_EDUx3IO_Gen4_mkI_Assembling_Guide.pdf)
- Download link at GitLab: https://gitlab.com/tinusaur/shield-edux3io-docs/-/raw/master/Tinusaur_Shield_EDUx3IO_Gen4_mkI_Assembling_Guide.pdf?inline=false

